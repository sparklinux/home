dwmblocks &
xrandr --output HDMI-2 --mode 1920x1080 --left-of HDMI-1 &
~/.local/bin/general/bgshuf &
setxkbmap -option caps:swapescape & nitrogen --restore &
xrdb -merge ~/.Xresources &
picom --experimental-backends --transparent-clipping &
lxsession &
pasystray &
/usr/lib/libexec/polkit-kde-authentication-agent-1 &

